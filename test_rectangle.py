from unittest import TestCase
from viewcontroller import Rectangle, Vector2

class TestRectangle(TestCase):
    def test_clone(self):
        r1 = Rectangle(0, 1, 2, 3)
        r2 = r1.clone()
        r2.size.x = 6
        r2.topleft.y = 3

        self.assertEquals(r1, Rectangle(0, 1, 2, 3))
        self.assertEquals(r2, Rectangle(0, 3, 6, 3))

    def test_bounds(self):
        r1 = Rectangle(3, 4, 10, 12)
        (v1, v2, v3, v4) = r1.bounds()

        self.assertEquals(v1, Vector2(3, 4))
        self.assertEquals(v2, Vector2(12, 4))
        self.assertEquals(v3, Vector2(3, 15))
        self.assertEquals(v4, Vector2(12, 15))

    def test_center(self):
        r1 = Rectangle(1, 2, 3, 5)
        self.assertEquals(r1.center(), Vector2(2, 4), str(r1.center()) + "!=" + str(Vector2(2, 4)))

        r2 = Rectangle(1, 2, 2, 4)
        self.assertEquals(r2.center(), Vector2(2, 4))

    def test_align(self):
        r1 = Rectangle(3, 5, 3, 6)
        r2 = Rectangle(1, 2, 4, 5)

        e1 = Rectangle(3, 2, 4, 5)
        o1 = r1.align(r2, alignment=Rectangle.LEFT)
        self.assertEquals(o1, e1, str(o1) + '!=' + str(e1))

        e2 = Rectangle(1, 2, 3, 6)
        o2 = r1.align(r2, stretch=Rectangle.HFILL | Rectangle.VFILL)
        self.assertEquals(o2, e2, str(o2) + "!=" + str(e2))

        e3 = Rectangle(2, 6, 4, 5)
        o3 = r1.align(r2, alignment=Rectangle.RIGHT | Rectangle.BOTTOM)
        self.assertEquals(o3, e3, str(o3) + '!=' + str(e3))

        e4 = Rectangle(1, 5, 4, 5)
        o4 = r1.align(r2, alignment=Rectangle.TOP)
        self.assertEquals(o4, e4, str(o4) + '!=' + str(e4))

    def test_translate(self):
        r1 = Rectangle(1, 2, 3, 4)

        e1 = Rectangle(6, 8, 3, 4)
        o1 = r1.clone()
        o1.translate(Vector2(5, 6))
        self.assertEquals(o1, e1, str(o1) + '!=' + str(e1))
