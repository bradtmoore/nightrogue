# game-specific ui
# essentially fills out viewcontroller in a specific way

# du du dumb
from viewcontroller import *
import libtcodpy as tdl
from model import *

#Title
#Options
#Save
#Load
#New


class BattleMapContext(Context):
    def __init__(self, engine):
        self.engine = engine
        super().__init__()

    def check_input(self, evt, key, mouse):
    # TODO make a keymap set and have this initialized by it so keys are remappable
        if evt == tdl.EVENT_KEY_RELEASE:
            dire = Vector2(0, 0)
            move = False
            if key.vk == tdl.KEY_UP:
                dire = Vector2(0,-1)
                move = True
            elif key.vk == tdl.KEY_DOWN:
                dire = Vector2(0,1)
                move = True
            elif key.vk == tdl.KEY_LEFT:
                dire = Vector2(-1,0)
                move = True
            elif key.vk == tdl.KEY_RIGHT:
                dire = Vector2(1,0)
                move = True
            if move:
                new_location = self.engine.player.location.location.clone().add(dire)
                if IsPassable(new_location).broadcast(self.engine).passable:
                    MoveLocation(self.engine.player, new_location).broadcast(self.engine)




class BattleMapConsole(Console):
    def __init__(self, engine):
        self.map_h = None
        self.map_w = None
        self.engine = engine
        self.engine.map_changed.register(self.handle_map_changed)
        self.engine.renderable_changed.register(self.handle_renderable_changed)
        self.bg_colchar = ColoredCharacter(' ', Color(0,0,0), Color(0,0,0))
        super().__init__(Rectangle(0,0,25,25), fill=self.bg_colchar)
        self.background = [self.bg_colchar for x in range(self.bounds.size.x * self.bounds.size.y)]

    def handle_renderable_changed(self, location):
        self.render_at(location.x, location.y, self.engine.get_renderable(location.x, location.y))

    def handle_map_changed(self, m):
        """
        The way this is implemented means the engine should load a map after this is constructed
        :param m:
        :return:
        """
        self.map_w = m.w
        self.map_h = m.h
        self._buffer.load(self.background)
        for x in range(self.map_w):
            for y in range(self.map_h):
                self.render_at(x, y, self.engine.get_renderable(x, y))

    def render_at(self, x, y, renderable):
        """
        Filters through all the renderable objects at a location at decides the final colchar to be displayed
        :param x:
        :param y:
        :param renderable:
        :return:
        """
        chars = [(obj.renderable.console_layer, obj.renderable.console_colchar) for obj in renderable.objects]
        chars.sort(key=lambda z: z[0], reverse=True)
        self.set(x, y, chars[0][1])
