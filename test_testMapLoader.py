from unittest import TestCase
from model import *
from engine import *
from util import *


class TestTestMapLoader(TestCase):
    def test_get_map(self):
        self.fail()

    def test_text_to_terrain(self):
        text = '''###
.#.
#.#'''
        terrain = TestMapLoader.text_to_terrain(text)
        self.assertEqual([str(x) for x in terrain], ['#', '#', '#', '.', '#', '.', '#', '.', '#'])

    def test_char_to_terrain(self):
        t1 = TestMapLoader.char_to_terrain('#', Vector2(3,5))
        self.assertEqual(Terrain.WALL, t1.terrain.type)
        self.assertEqual(Vector2(3, 5), t1.location.location)
