from viewcontroller import *
from libtcodpy import *
from util import *

con = 0
SCREEN_WIDTH = 80
SCREEN_HEIGHT = 45
td = LibTCODDrawer(SCREEN_WIDTH, SCREEN_HEIGHT)
c = Console(Rectangle(0, 0, 15, 15), fill=ColoredCharacter('.', Color(0,0,0), Color(255,255,255)))
c2 = Console(Rectangle(5, 5, 5, 5), fill=ColoredCharacter('X', Color(255, 0, 0), Color(255, 255, 255)))
c.add(c2)
c3 = Console(Rectangle(0, 0, 2, 2), fill=ColoredCharacter('@', Color(0, 255, 0), Color(255,255,255)))
c2.add(c3)
c.flush()
while not console_is_window_closed():
    #c.flush()
    c.draw(td) # without flushing, this simply draws the dirty parts of the buffer
    td.flush()
    key = console_wait_for_keypress(True)
    if key.vk == KEY_SPACE:
        c3.set(1,1,ColoredCharacter('%', Color(0, 0, 255), Color(0,0,0)))
        print('Pressed!')
