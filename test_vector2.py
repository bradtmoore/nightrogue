from unittest import TestCase
from viewcontroller import Vector2

class TestVector2(TestCase):
    def test_add(self):
        v1 = Vector2(3, 7)
        v2 = Vector2(4, 5)

        self.assertEquals(v1.add(v2), Vector2(7, 12))

    def test_sub(self):
        v1 = Vector2(3, -1)
        v2 = Vector2(-1, 1)

        self.assertEquals(v2.sub(v1), Vector2(-4, 2))

    def test_prod(self):
        v1 = Vector2(3, 6)
        v2 = Vector2(-1, 2)

        self.assertEquals(v1.prod(v2), Vector2(-3, 12))

    def test_clone(self):
        v1 = Vector2(2, 4)
        v2 = v1.clone()
        v2.x = 6

        self.assertEquals(v2, Vector2(6, 4))
        self.assertEquals(v1, Vector2(2, 4))
