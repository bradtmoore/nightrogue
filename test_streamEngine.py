from unittest import TestCase
from engine import *


class TestRule1(GlobalStreamFunction):
    def __init__(self):
        super().__init__(TestEvent1)

    def process(self, strdata, engine):
        strdata.msg += 'Howdy!'
        return strdata


class TestEvent1(StreamData):
    streamspec = OrderedStreamSpec([TestRule1])

    def __init__(self):
        self.msg = 'Yeah!'
        super().__init__(TestEvent1.streamspec)


class TestRule2(ObjectStreamFunction):
    def __init__(self, obj):
        super().__init__(TestEvent2, obj)

    def process(self, strdata, engine):
        strdata.msg = "Hey you: " + str(type(self.obj))
        return strdata


class TestEvent2(ObjectStreamData):
    streamspec = OrderedStreamSpec([TestRule2])

    def __init__(self, obj):
        self.msg = None
        super().__init__(TestEvent2.streamspec, obj)


class TestRule3(AbsoluteLocationStreamFunction):
    def __init__(self, location):
        super().__init__(location, TestEvent3)

    def process(self, strdata, engine):
        strdata.msg += str(self.location)


class TestEvent3(LocationStreamData):
    streamspec = OrderedStreamSpec([TestRule3])

    def __init__(self, location):
        self.msg = 'Position='
        super().__init__(TestEvent3.streamspec, location)


class TestStreamEngine(TestCase):
    def test_broadcast(self):
        te = TestEvent1()
        engine = StreamEngine()
        engine.register(TestRule1())
        x = te.broadcast(engine)
        self.assertEqual('Yeah!Howdy!', te.msg)

    def test_broadcast2(self):
        go = GameObject([])
        te = TestEvent2(go)
        tr = TestRule2(go)
        go.set_rules([tr])
        engine = StreamEngine()
        go.register(engine)
        te.broadcast(engine)
        self.assertEqual('Hey you: <class \'engine.GameObject\'>', te.msg)

    def test_broadcast3(self):
        m = Map(25, 25)
        te = TestEvent3(Vector2(2, 7))
        tr = TestRule3(Vector2(2, 7))
        engine = StreamEngine()
        engine.set_map(m)
        engine.register(tr)
        te.broadcast(engine)
        self.assertEqual('Position=(2,7)', te.msg)
