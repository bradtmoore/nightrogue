from model import BattleEngine, TestMapLoader
from ui import BattleMapConsole, BattleMapContext, ContextList
from viewcontroller import LibTCODDrawer, TextBox, ColoredCharacter
import libtcodpy as tdl
from util import *
from time import time

def sys_ms():
    return int(round(time()*1000))

drawer = LibTCODDrawer(30, 30)
engine = BattleEngine()
console = BattleMapConsole(engine)
context = BattleMapContext(engine)
cl = ContextList([context])
engine.set_map(TestMapLoader().get_map())
fps_console = TextBox(
    Rectangle(0,15,10,10),
    ColoredCharacter(' ', tdl.Color(0,0,0), tdl.Color(0,0,0)),
    ColoredCharacter('.', tdl.Color(255,0,0), tdl.Color(0,0,0)))
console.add(fps_console)
t1 = sys_ms()
t2 = t1
fps = 1000
while not tdl.console_is_window_closed():
    t2 = sys_ms()
    dt = t2 - t1
    t1 = t2
    fps = 1000/dt if dt > 0 else 1000
    fps_console.update_text(str.format('fps:{0:4g}', fps))
    console.draw(drawer)
    drawer.flush()
    cl.check_input()