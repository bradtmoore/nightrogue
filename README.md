# README #

A game demo written in Python with SDL2.  Has a neat stream processing publish/subscribe engine for defining game rules and turn order.  This is an older demo; I'm currently working on a better version of the engine.
  
### Files ###
* itest_* are .py files that can be executed (itest stands for interactive test).
* test_* are .py files for unit testing
* game.py is the demo executable

Note, I've developed and executed this in PyCharm.  I hope there isn't any issues running it with just the native interpreter (please let me know).