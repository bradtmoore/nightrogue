import inspect
import sys
import re
from util import Matrix, Vector2


class StreamEngine:
    def __init__(self):
        self.map = None
        self.global_dispatcher = GlobalDispatcher(self)
        self.object_dispatcher = ObjectDispatcher(self)
        self.location_dispatcher = None

    def register(self, strfunc):
        self.get_dispatcher(type(strfunc)).register(strfunc)

    def dispatch(self, strdata, strfunc_cls):
        return self.get_dispatcher(strfunc_cls).dispatch(strdata, strfunc_cls)

    def get_dispatcher(self, strfunc_cls):
        if issubclass(strfunc_cls, ObjectStreamFunction):
            ans = self.object_dispatcher
        elif issubclass(strfunc_cls, LocationStreamFunction):
            ans = self.location_dispatcher
        else:
            ans = self.global_dispatcher
        return ans

    def remove(self, strfunc):
        self.get_dispatcher(type(strfunc)).remove(strfunc)

    def set_map(self, m):
        """
        Resets and initializes the location dispatcher according to size of m
        :param m:
        :return:
        """
        self.map = m
        self.location_dispatcher = LocationDispatcher(m.w, m.h, self)

    def clear(self):
        self.global_dispatcher = GlobalDispatcher(self)
        self.object_dispatcher = ObjectDispatcher(self)
        self.location_dispatcher = None
        self.map = None


class StreamData:
    """
    Represents a mutable data stream or event
    """
    def __init__(self, streamspec):
        """
        :param streamspec: The streamspec defining the handling of this event.  Typically static
         in the derived class.
        """
        self.streamspec = streamspec

    def broadcast(self, engine):
        """
        Uses the engine to transmit and subsequently return this data stream
        :param engine: an instance of StreamEngine
        :return:
        """
        return self.streamspec.broadcast(self, engine)


class StreamSpec:
    """
    This class may be optional.  I think OrderStreamSpec is really the only way to do this.
    """
    def broadcast(self, strdata, engine):
        raise NotImplementedError


class OrderedStreamSpec(StreamSpec):
    """
    Specifies the handling of a data stream as a list of stream functions to follow in order.
    """
    def __init__(self, strfunc_clss):
        """

        :param strfunc_clss: a list of StreamFunction subclasses
        """
        self.strfunc_clss = strfunc_clss

    def broadcast(self, strdata, engine):
        """
        Transmits the give data stream to the registered stream functions in engine according to the
        specification of this stream
        :param strdata: the stream data to transmit
        :param engine: the engine holding the instances of stream functions
        :return:
        """
        for f in self.strfunc_clss:
            engine.dispatch(strdata, f)
        return strdata


class Dispatcher:
    def __init__(self, engine):
        self.engine = engine

    def dispatch(self, strdata, strfunc_cls):
        raise NotImplementedError

    def register(self, strfunc):
        raise NotImplementedError

    def remove(self, strfunc):
        raise NotImplementedError


class GlobalDispatcher(Dispatcher):
    """
    Dispatches stream data to every registered function
    """
    def __init__(self, engine):
        self._map = {}
        super().__init__(engine)

    def dispatch(self, strdata, strfunc_cls):
        for f in self._map[strfunc_cls]:
            f.process(strdata, self.engine)
        return strdata

    def register(self, strfunc):
        self._map.setdefault(type(strfunc), []).append(strfunc)

    def remove(self, strfunc):
        self._map[type(strfunc)].remove(strfunc)


class ObjectStreamData(StreamData):
    def __init__(self, streamspec, obj=None):
        self.obj = obj
        super().__init__(streamspec)


class ObjectDispatcher(Dispatcher):
    """
    Dispatches stream data to ever registered function to its target
    """
    def __init__(self, engine):
        self._map = {}
        super().__init__(engine)

    def register(self, strfunc):
        self._map.setdefault((strfunc.obj, type(strfunc)), []).append(strfunc)

    def remove(self, strfunc):
        self._map[(strfunc.obj, type(strfunc))].remove(strfunc)

    def dispatch(self, strdata, strfunc_cls):
        for f in self._map[(strdata.obj, strfunc_cls)]:
            f.process(strdata, self.engine)
        return strdata


class StreamFunction:
    """
    Represents a process/rule that will receive and possibly modify stream data
    """
    def __init__(self, strdata_cls):
        self.strdata_cls = strdata_cls

    def process(self, strdata, engine):
        raise NotImplementedError


class GlobalStreamFunction(StreamFunction):
    """
    A stream function that responds to any stream data
    """
    def __init__(self, strdata_cls):
        super().__init__(strdata_cls)


class ObjectStreamFunction(StreamFunction):
    def __init__(self, strdata_cls, obj):
        self.obj = obj
        super().__init__(strdata_cls)


class Map:
    def __init__(self, w, h):
        self.w = w
        self.h = h


class GameStruct:
    def __init__(self, name):
        self.name = name


class GameObject:
    """
    A game object is a collection of data (GameStructs) and rules that act on events.
    """
    NEXT_ID = 1

    @staticmethod
    def get_next_id():
        x = GameObject.NEXT_ID
        GameObject.NEXT_ID += 1
        return x

    def __init__(self, structs, rules):
        self.id = GameObject.get_next_id()
        self.__dict__.update({s.name: s for s in structs})  # turn our structs into proper members
        self.rules = rules

    def set_rules(self, rules):
        """
        I think there was a good reason to set the rules after the fact, but I don't remember it.
        :param rules:
        :return:
        """
        self.rules = rules

    def register(self, engine):
        for r in self.rules:
            engine.register(r)

    def remove(self, engine):
        for r in self.rules:
            engine.remove(r)


class LocationStreamFunction(StreamFunction):
    def __init__(self, strdata_cls, area=None):
        """

        :param strdata_cls:
        :param area: list of points relative to this objects location where the stream function is under effect
        """
        self.area = [Vector2(0,0)] if area is None else area
        super().__init__(strdata_cls)

    @property
    def location(self):
        raise NotImplementedError

    def region(self):
        """
        Returns the actual grid point where this stream function is in effect

        :return: a list of Vector2
        """
        x0 = self.location.x
        y0 = self.location.y
        return [Vector2(x0 + i.x, y0 + i.y) for i in self.area]


class AbsoluteLocationStreamFunction(LocationStreamFunction):
    def __init__(self, location, strdata_cls, area=None):
        self._location = location
        super().__init__(strdata_cls, area)

    @property
    def location(self):
        return self._location


class LocationStruct(GameStruct):
    def __init__(self, location=None):
        self.location = Vector2(0,0) if location is None else location
        super().__init__('location')


class ObjectLocationStreamFunction(LocationStreamFunction):
    """
    A rule that is based on its object's location.
    """
    def __init__(self, obj, strdata_cls, area=None):
        self.obj = obj
        super().__init__(strdata_cls, area)

    @property
    def location(self):
        return self.obj.location.location


class LocationStreamData(StreamData):
    def __init__(self, streamspec, location):
        self.location = location
        super().__init__(streamspec)


class LocationDispatcher(Dispatcher):
    """
    Take care to remove and register stream functions whose location changes.
    """
    def __init__(self, w, h, engine):
        self.matrix = Matrix(w, h, {})
        super().__init__(engine)

    def dispatch(self, strdata, strfunc_cls):
        """

        :param strdata: should derive from LocationStreamData
        :return:
        """
        try:
            for f in self.matrix.get(strdata.location.x, strdata.location.y)[strfunc_cls]:
                f.process(strdata, self.engine)
        except KeyError:
            pass # the rule just might not be at that location
        return strdata

    def register(self, strfunc):
        """

        :param strfunc: should derive from LocationStreamFunction
        :return:
        """
        for p in strfunc.region():
            self.matrix.get(p.x, p.y).setdefault(type(strfunc), []).append(strfunc)

    def remove(self, strfunc):
        """

        :param strfunc: should derive from LocationStreamFunction
        :return:
        """
        for p in strfunc.region():
            self.matrix.get(p.x, p.y)[type(strfunc)].remove(strfunc)



class RuleSet:
    """
    Recommend making a subclass of this, and then creating static constructor methods for different RuleSets,
    essentially making custom constructors for a set of rules.
    """
    def __init__(self, rules):
        self.rules = [] if rules is None else rules

    @classmethod
    def union(cls, *args):
        """
        Takes arguments of individual rules and RuleSets and returns a single RuleSet containing all
        :param args:
        :return:
        """
        ans = []
        for a in args:
            if isinstance(a, RuleSet):
                ans.append(a.rules)
            else:
                ans.append(a)
        return RuleSet(ans)
