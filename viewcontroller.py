import libtcodpy as libtcod
from libtcodpy import Color
from util import *
import textwrap
from math import floor, ceil


class Buffer:
    """
    Implements a buffered matrix that keeps track of which elements are dirty using
    a set.
    """
    def __init__(self, w, h, fill):
        self.w = w
        self.h = h
        self._vals = Matrix(w, h, fill)
        self._dirty = set()

    def load(self, data):
        self._vals.load(data)
        self.dirty_all()

    def get(self, x, y):
        return self._vals.get(x, y)

    def set(self, x, y, val):
        self._vals.set(x, y, val)
        self._dirty.add(Vector2(x, y))

    def dirty_all(self):
        self._dirty = {Vector2(x, y) for x in range(self.w) for y in range(self.h)}

    def dirty(self, rect):
        b = rect.bounds()
        x1 = b[0].x
        x2 = b[1].x
        y1 = b[0].y
        y2 = b[2].y
        self._dirty = {Vector2(x, y) for x in range(start=x1, stop=x2) for y in range(start=y1, stop=y2)}

    def flush_dirty(self, rect=None):
        if rect is None:
            ans = self._dirty
            self._dirty = set()
        else:
            ans = {v for v in self._dirty if rect.contains(v)}
            self._dirty = self._dirty.difference(ans)
        return ans

    def get_dirty(self):
        return self._dirty


class Console:
    """
    Represents the base UI widget.  A tiled representation of the game screen that can draw font characters with
    color onto the grid.  Also allows for child consoles.
    """
    def __init__(self, bounds, fill=None, children=None):
        self.bounds = bounds
        fill = ColoredCharacter(' ') if fill is None else fill
        self._buffer = Buffer(bounds.size.x, bounds.size.y, fill)
        children = [] if children is None else children
        self._children = children

    def load(self, data):
        self._buffer.load(data)

    def set(self, x, y, colchar):
        self._buffer.set(x, y, colchar)

    def draw(self, drawer, transform=None):
        """
        Draws the buffer of this console and its children
        :param drawer:
        :param transform: if this console is specified relative to something, this is the appropriate translation
        matrix
        :return:
        """
        transform = Vector2(0,0) if transform is None else transform
        t = self.bounds.topleft.clone().add(transform)
        for pt in self._buffer.flush_dirty():
            pt2 = pt.clone().add(t)
            drawer.draw(pt2.x, pt2.y, self._buffer.get(pt.x, pt.y))
        for c in self._children:
            c.draw(drawer, t)

    def add(self, c):
        """
        Add a child (some derivative of console) to this console (specified relative to this).
        :param c:
        :return:
        """
        self._children.append(c)

    def remove(self, c):
        """
        Remove a child from this console.
        :param c:
        :return:
        """
        self._children.remove(c)

    def flush(self):
        """
        Causes this buffer and its children to redraw everything on the next call to draw()
        :return:
        """
        self._buffer.dirty_all()
        for c in self._children:
            c.flush()

    def add_tween(self, tween):
        raise NotImplementedError

    def remove_tween(self, tween):
        raise NotImplementedError

    def update(self, dt):
        # for tween in tweens do update with self (will dirty buffer allowing next draw to do it)
        raise NotImplementedError


class Tween:
    # TODO update or figure out tweens, though I like this skeleton
    def update(self, dt, c):
        """
        Updates/sets cells on the given console
        :param dt: delta time in ms
        :param c: console to update on
        :return:
        """
        raise NotImplementedError


class BlinkingTween(Tween):
    def __init__(self, colchar1, colchar2, delta):
        raise NotImplementedError

# class ScissorConsole(Console):
#     def __init__(self, rect, buffer, padding=None):
#         super.__init__(rect, padding)
#         self._buffer = buffer
#         self._buffer_rect = Rectangle(0, 0, buffer.w, buffer.h)
#         self._view = self._child_rect.clone()
#
#     def move_view(self, v):
#         self._view.translate(v.sub(self._view.topleft))
#         self._buffer.dirty(self._view)


class ColoredCharacter:
    def __init__(self, char, fgcolor=Color(0,0,0), bgcolor=Color(255,255,255)):
        self.char = char
        self.fgcolor = fgcolor
        self.bgcolor = bgcolor


class GridDrawer:
    def draw(self, x, y, colchar):
        """
        Draw the colored character at the specified screen cell.
        :param x:
        :param y:
        :param colchar:
        :return:
        """
        raise NotImplementedError


class LibTCODDrawer(GridDrawer):
    def __init__(self, ncol, nrow, title='Test', font_size=24):
        fp = 'fonts/terminal' + str(font_size) + 'x' + str(font_size) + '_gs_ro.png'
        libtcod.console_set_custom_font(
            fp.encode('utf-8'), libtcod.FONT_TYPE_GREYSCALE | libtcod.FONT_LAYOUT_ASCII_INROW)
        libtcod.console_init_root(ncol, nrow, title.encode('utf-8'), renderer=libtcod.RENDERER_OPENGL)

    def draw(self, x, y, colchar):
        libtcod.console_put_char_ex(
            0, x, y, colchar.char.encode('cp437'), colchar.fgcolor, colchar.bgcolor)

    def flush(self):
        libtcod.console_flush()


class Context:
    def check_input(self, evt, key, mouse):
        """
        May be blocking or not blocking.
        :return: a boolean whether to pass the event on (true = pass, false = stop)
        """
        # TODO implement an input adapter for this
        raise NotImplementedError


class ContextList:
    def __init__(self, stack=None):
        self.stack = [] if stack is None else stack
        self.key = libtcod.Key()
        self.mouse = libtcod.Mouse()

    def push(self, context):
        self.stack.insert(0, context)

    def remove(self, context):
        self.stack.remove(context)

    def check_input(self):
        evt = libtcod.sys_check_for_event(libtcod.EVENT_ANY, self.key, self.mouse)
        if evt != 0:
            for c in self.stack:
                if not c.check_input(evt, self.key, self.mouse):
                    break


class TextCannotFitError(Exception):
    def __init__(self, text, size):
        super().__init__('The text: {0}, cannot fit in {1}', text, str(size))


class TextBox(Console):
    def __init__(self, bounds, bg_colchar, text_colchar, text=None, wraptext=True, editable=False, cursor_colchar=None):
        self.bounds = bounds
        self.bg_colchar = bg_colchar
        self.text_colchar = text_colchar
        self.text = '' if text is None else text
        self.wraptext = wraptext
        self.editable = editable

        if cursor_colchar is None:
            self.cursor_colchar = ColoredCharacter(b'\xFE'.decode('cp437'), self.bg_colchar.fgcolor, self.bg_colchar.bgcolor)
        self.wrapper = textwrap.TextWrapper(
            width=self.bounds.size.x,
            drop_whitespace=False if editable else True,
            replace_whitespace=False)

        super().__init__(bounds, fill=self.bg_colchar)

    def update_text(self, text):
        if self.wraptext:
            wrapped = self.wrapper.wrap(text)
        else:
            wrapped = text.split('\n')
        if len(wrapped) > self.bounds.size.y:
            raise TextCannotFitError(self.text, self.bounds)

        self.text = text
        self.load([self.bg_colchar for x in range(self.bounds.size.x * self.bounds.size.y)])
        for y in range(len(wrapped)):
            line = wrapped[y]
            for x in range(len(line)):
                c = line[x]
                self.set(x, y, ColoredCharacter(c, self.text_colchar.fgcolor, self.text_colchar.bgcolor))

        # solely to place the cursor
        if self.editable:
            if len(wrapped) > 0:
                y1 = len(wrapped) - 1
                x1 = len(wrapped[y1])
            else:  # handles case when string is empty
                x1 = 0
                y1 = 0

            if x1 >= self.bounds.size.x:
                y1 += 1
                if y1 < self.bounds.size.y:
                    x1 = 0
            if x1 < self.bounds.size.x and y1 < self.bounds.size.y:
                self.set(x1, y1, self.cursor_colchar)


class TextBoxContext(Context):
    def __init__(self, textbox):
        self.textbox = textbox
        self.sig_exit = Signal()

    def check_input(self, evt, key, mouse):
        ans = True
        if evt == libtcod.EVENT_KEY_RELEASE:
            if key.vk == libtcod.KEY_BACKSPACE: # needs to be before c != 0 check
                t1 = self.textbox.text
                if len(t1) > 0:
                    t2 = t1[0:(len(t1)-1)]
                    self.textbox.update_text(t2)
            elif key.vk == libtcod.KEY_ENTER:
                self.sig_exit.fire(True, self.textbox.text)
            elif key.vk == libtcod.KEY_ESCAPE:
                self.sig_exit.fire(False, None)
            elif key.c != 0:
                char = key.c.to_bytes(1, 'big').decode('cp437')
                try:
                    self.textbox.update_text(self.textbox.text + char)
                except TextCannotFitError:
                    pass
            ans = False



