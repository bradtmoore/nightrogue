from viewcontroller import *

drawer = LibTCODDrawer(10, 10)
tb = TextBox(
    Rectangle(0,0,8,2),
    ColoredCharacter('%', Color(255,255,255), Color(0,0,0)),
    ColoredCharacter('%', Color(255,255,255), Color(0,0,0)),
    editable=True,
    wraptext=False)
tb.update_text('Hiya!\n My Man!')
tbc = TextBoxContext(tb)
cl = ContextList([tbc])

def text_handler(success, text):
    if success:
        print(text)
    else:
        print("USER CANCELED")
    global cl, tbc
    cl.remove(tbc)

tbc.sig_exit.register(text_handler)
while not libtcod.console_is_window_closed():
    tb.draw(drawer)
    drawer.flush()
    cl.check_input()