# class Singleton(type):
#     def __init__(self, *args, **kwargs):
#         super(Singleton, self).__init__(*args, **kwargs)
#         self.__instance = None
#     def __call__(self, *args, **kwargs):
#         if self.__instance is None:
#             self.__instance = super(Singleton, self).__call__(*args, **kwargs)
#         return self.__instance
from copy import copy, deepcopy

class Vector2:
    """"
    Quick and simple vector/point class.  Realized that numpy would be way overkill for a fixed-grid game
    with rectangular windows.
    """

    def __init__(self, x=0, y=0):
        """
        x and y are cartesian with the origin top left
        """
        self.x = x
        self.y = y

    def __eq__(self, other):
        if not isinstance(other, self.__class__):
            return NotImplemented
        return self.x == other.x and self.y == other.y

    def __hash__(self):
        return hash(self.x) ^ hash(self.y)

    def __str__(self):
        return "(" + str(self.x) + "," + str(self.y) + ")"

    def add(self, v1):
        return Vector2(self.x + v1.x, self.y + v1.y)

    def sub(self, v1):
        return self.add(Vector2(-v1.x, -v1.y))

    def prod(self, v1):
        return Vector2(self.x * v1.x, self.y * v1.y)

    def clone(self):
        return Vector2(self.x, self.y)


class Matrix:
    """
    Non-resizable cheap easy matrix class
    """

    def __init__(self, w, h, fill=0, copy_fill=True):
        self.w = w
        self.h = h
        if copy_fill:
            self._vals = [([copy(fill) for c in range(w)]) for r in range(h)]
        else:
            self._vals = [([fill for c in range(w)]) for r in range(h)]
        self._x = None
        self._y = None

    def get(self, x, y):
        return self._vals[y][x]

    def set(self, x, y, val):
        self._vals[y][x] = val

    def load(self, data):
        i = 0
        for y in range(self.h):
            for x in range(self.w):
                self.set(x, y, data[i])
                i += 1

    def __iter__(self):
        self._x = 0
        self._y = 0
        return self

    def __next__(self):
        if 0 <= self._x < self.w and 0 <= self._y < self.h:
            ans = self.get(self._x, self._y)
            if self._x == self.w - 1:
                self._x = 0
                self._y += 1
            else:
                self._x += 1
            return ans
        raise StopIteration

class Rectangle:
    """
    Rectangle class that allows for aligning other rectangles relative to each other.  It also provides
    convenience methods for bounds and the like.  The idea is to use it to store the location of consoles
    and rectangles on the map.
    """

    def __init__(self, x, y, w, h):
        """
        x and y are cartesian with the origin top left
        """
        self.topleft = Vector2(x, y)
        self.size = Vector2(w, h)
        pass

    def __eq__(self, other):
        if not isinstance(other, self.__class__):
            return NotImplemented
        return self.topleft == other.topleft and self.size == other.size

    def __hash__(self):
        return hash(self.topleft) ^ hash(self.size)

    def __str__(self):
        return "{" + str(self.topleft) + "," + str(self.size) + "}"

    def clone(self):
        return Rectangle(self.topleft.x, self.topleft.y, self.size.x, self.size.y)

    def bounds(self):
        """
        Compute the corners of this rectangle
        :return: a tuple of Vector2 points (top-left, top-right, bottom-left, bottom-right)
        """
        x = self.topleft.x
        y = self.topleft.y
        w = self.size.x
        h = self.size.y
        return Vector2(x, y), Vector2(x + w - 1, y), Vector2(x, y + h - 1), Vector2(x + w - 1, y + h - 1)

    def center(self):
        """
        Compute the center of this rectangle.  Note that this implementation rounds down to the nearest grid point.
        :return:
        """
        ans = self.topleft.add(self.size.prod(Vector2(0.5, 0.5)))
        ans.x = floor(ans.x)
        ans.y = floor(ans.y)
        return ans

    NONE = 0
    TOP = 1
    BOTTOM = 2
    VCENTER = 4
    LEFT = 8
    RIGHT = 16
    HCENTER = 32
    HFILL = 1
    VFILL = 2

    def align(self, src, alignment=NONE, stretch=NONE):
        ans = src.clone()
        if stretch & Rectangle.HFILL:
            ans.size.x = self.size.x

        if stretch & Rectangle.VFILL:
            ans.size.y = self.size.y

        if alignment & Rectangle.TOP:
            ans.topleft.y = self.topleft.y

        if alignment & Rectangle.BOTTOM:
            ans.topleft.y = self.bounds()[3].y - ans.size.y + 1

        def alv():
            return self.center().sub(src.center())

        if alignment & Rectangle.VCENTER:  # could be bigger or smaller
            ans.topleft.y += alv().y

        if alignment & Rectangle.LEFT:
            ans.topleft.x = self.topleft.x

        if alignment & Rectangle.RIGHT:
            ans.topleft.x = self.bounds()[1].x - ans.size.x + 1

        if alignment & Rectangle.HCENTER:
            ans.topleft.x += alv().x

        return ans

    def translate(self, v1):
        self.topleft = self.topleft.add(v1)

    def contains(self, v1):
        b = self.bounds()
        x1 = b[0].x
        x2 = b[1].x
        y1 = b[0].y
        y2 = b[2].y
        return x1 <= v1.x <= x2 and y1 <= v1.y <= y2


class Signal:
    """
    A general purpose object/observer pattern.
    """
    def __init__(self):
        self.bound_handlers = []

    def register(self, bhandler):
        """
        Registers a bound function to call when this signal is fired
        :param bhandler:
        :return:
        """
        self.bound_handlers.append(bhandler)

    def remove(self, bhandler):
        self.bound_handlers.remove(bhandler)

    def fire(self, *args):
        for bh in self.bound_handlers:
            bh(*args)
# Quad Trees?


