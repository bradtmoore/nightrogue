from util import *
from engine import *
from math import floor
from viewcontroller import ColoredCharacter
from libtcodpy import Color


class ContentsStruct(GameStruct):
    def __init__(self, contents=None):
        self.contents = [] if contents is None else contents
        super().__init__('contents')


class ObjectImpassibleRule(ObjectLocationStreamFunction):
    """
    Thinking of separating the notion of impassible with attackable and swappable (ally).  So, even allied
    NPCs would be impassible (but swappable).
    """
    def __init__(self, obj):
        super().__init__(obj, IsPassable)

    def process(self, strdata, engine):
        strdata.passable = False
        return strdata


class OutOfBoundsImpassible(GlobalStreamFunction):
    def __init__(self):
        super().__init__(IsPassable)

    def process(self, strdata, engine):
        x = strdata.location.x
        y = strdata.location.y
        m = engine.get_map()
        strdata.passable = strdata.passable and (0 <= x < m.w and 0 <= y < m.h)


class IsPassable(LocationStreamData):
    streamspec = OrderedStreamSpec([OutOfBoundsImpassible, ObjectImpassibleRule])

    def __init__(self, location):
        self.passable = True
        super().__init__(IsPassable.streamspec, location)


class ReportRenderableAt(ObjectLocationStreamFunction):
    """
    An object reports that it is renderable (and its renderable struct) at this location
    """
    def __init__(self, obj):
        self.obj = obj
        super().__init__(obj, RenderableAt)

    def process(self, strdata, engine):
        strdata.objects.append(self.obj)
        return strdata


class RenderableAt(LocationStreamData):
    """
    An event asking for all things renderable at a certain location
    """
    streamspec = OrderedStreamSpec([ReportRenderableAt])

    def __init__(self, location):
        self.objects = []
        super().__init__(RenderableAt.streamspec, location)


class ReportRenderableAfterLocationChange(GlobalStreamFunction):
    def __init__(self):
        super().__init__(MoveLocation)

    def process(self, strdata, engine):
        engine.renderable_changed.fire(strdata.old_location)
        engine.renderable_changed.fire(strdata.new_location)

# # TODO add logic for when game objects are placed or removed off the map


class ChangeLocationOnMove(ObjectStreamFunction):
    def __init__(self, obj):
        super().__init__(MoveLocation, obj)

    def process(self, strdata, engine):
        loc0 = self.obj.location.location
        loc1 = strdata.new_location
        self.obj.location.location = loc1


class RemoveOldRulesOnMove(GlobalStreamFunction):
    def __init__(self):
        super().__init__(MoveLocation)

    def process(self, strdata, engine):
        for r in strdata.obj.rules:
            if isinstance(r, LocationStreamFunction):
                engine.remove(r)
        return strdata


class RestoreRulesOnMove(GlobalStreamFunction):
    def __init__(self):
        super().__init__(MoveLocation)

    def process(self, strdata, engine):
        for r in strdata.obj.rules:
            if isinstance(r, LocationStreamFunction):
                engine.register(r)


class MoveLocation(StreamData):
    streamspec = OrderedStreamSpec([RemoveOldRulesOnMove, ChangeLocationOnMove, RestoreRulesOnMove, ReportRenderableAfterLocationChange])

    def __init__(self, obj, new_location):
        self.obj = obj
        self.old_location = self.obj.location.location
        self.new_location = new_location
        super().__init__(MoveLocation.streamspec)


class BattleMap(Map):
    """
    As opposed to an overworld map or the like.  The standard game map.
    """
    def __init__(self, w, h, terrain=None, objects=None):
        self.terrain = Matrix(w, h) if terrain is None else terrain
        self.objects = [] if objects is None else objects
        super().__init__(w, h)

    def register(self, engine):
        for t in self.terrain:
            t.register(engine)
        for obj in self.objects:
            obj.register(engine)


class RenderableStruct(GameStruct):
    def __init__(self, console_layer, console_colchar):
        self.console_layer = console_layer
        self.console_colchar = console_colchar
        super().__init__('renderable')


class Terrain(GameObject):
    def __init__(self, location, console_colchar, structs, rules):
        structs.append(RenderableStruct(0, console_colchar))
        structs.append(LocationStruct(location))
        rules.append(ReportRenderableAt(self))
        super().__init__(structs, rules)


class GroundTerrain(Terrain):
    def __init__(self, location):
        super().__init__(location,
                         ColoredCharacter('.', Color(255,255,255), Color(0,0,0)),
                         [],
                         [])


class WallTerrain(Terrain):
    def __init__(self, location):
        super().__init__(location,
                         ColoredCharacter('#', Color(0,0,255), Color(0,0,0)),
                         [],
                         [ObjectImpassibleRule(self)])


class Player(GameObject):
    def __init__(self, location):
        super().__init__(
            [
                LocationStruct(location),
                RenderableStruct(1, ColoredCharacter('@', Color(255,255,255), Color(0,0,0)))
            ],
            [
                ReportRenderableAt(self),
                ChangeLocationOnMove(self)
            ])




class TestMapLoader:
    def __init__(self):
        pass

    def get_map(self):
        m = '''###########
#.........#
#.........#
#....#....#
#.........#
###########'''
        terrain = TestMapLoader.text_to_terrain(m)
        p = Player(Vector2(1, 1))
        return BattleMap(terrain.w, terrain.h, terrain, [p])

    @staticmethod
    def text_to_terrain(text):
        w = text.find('\n')
        line = text.replace('\n', '')
        n = len(line)
        h = floor(n / w)
        ans = Matrix(w, h)
        ans.load([TestMapLoader.char_to_terrain(line[i], Vector2(i % w, floor(i / w))) for i in range(n)])
        return ans

    @staticmethod
    def char_to_terrain(c, location):
        ans = None
        if c == '#':
            ans = WallTerrain(location)
        elif c == '.':
            ans = GroundTerrain(location)
        return ans


class BattleEngine(StreamEngine):
    def __init__(self):
        super().__init__()
        self.player = None
        self.renderable_changed = Signal()
        self.map_changed = Signal()
        self.register(ReportRenderableAfterLocationChange())
        self.register(RemoveOldRulesOnMove())
        self.register(RestoreRulesOnMove())
        self.register(OutOfBoundsImpassible())

    def get_renderable(self, x, y):
        return RenderableAt(Vector2(x, y)).broadcast(self)

    def set_map(self, m):
        super().set_map(m)
        self.player = [obj for obj in m.objects if isinstance(obj, Player)][0]
        m.register(self)
        self.fire_map_changed(m)

    def fire_map_changed(self, m):
        self.map_changed.fire(m)

    def fire_renderable_changed(self, locations):
        for loc in locations:
            self.renderable_changed.fire(loc)

    def get_map(self):
        return self.map

